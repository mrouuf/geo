package com.example.geo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface GetDataServices {
    @GET
    Call<ModelWeather> getData(@Url String url);
}
