package com.example.geo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView itemCategory;
    private TextView elevationMain, lngMain, observationMain
            , ICAO, clouds, dewPoint, cloudsCode, datetime, countryCode
            , temperature, humidity, stationName, weatherCondition
            , windDirection, hectoPascAltimeter, windSpeed, lat, username, inputicao;
    private RecyclerView recyclerView;
    private GetDataServices service;
    //    DataModel dataModel;
    private List<DataWeather> weatherList;
    ProgressDialog progressDialog;
    private String valueusername, valueicao;
    ModelWeather modelWeather;
    private Button submit;
    GetDataServices getDataServices;
    public static MainActivity ma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getDataServices = RetrofitClient.getRetrofitINstance().create(GetDataServices.class);
        ma= this;
        setupView();
    }

    private void setupView() {
        elevationMain = findViewById(R.id.tv_elevation);
        lngMain = findViewById(R.id.tv_lng);
        username = findViewById(R.id.inputusername);
        inputicao = findViewById(R.id.inputicao);
        submit = findViewById(R.id.submit);
        observationMain = findViewById(R.id.tv_observation);
        ICAO = findViewById(R.id.tv_icao);
        clouds = findViewById(R.id.tv_clouds);
        dewPoint = findViewById(R.id.tv_dewpoint);
        cloudsCode = findViewById(R.id.tv_clouds_code);
        datetime = findViewById(R.id.tv_datetime);
        countryCode = findViewById(R.id.tv_country_code);
        temperature = findViewById(R.id.tv_temperature);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                networkRequest();
            }
        });


    }

    private void networkRequest() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        final String inputUsername = username.getText().toString();
        final String inputICAO = inputicao.getText().toString();


        service = RetrofitClient.getRetrofitINstance().create(GetDataServices.class);
        service.getData("http://api.geonames.org/weatherIcaoJSON?ICAO="+inputICAO+"&username="+inputUsername).enqueue(new Callback<ModelWeather>() {
            @Override
            public void onResponse(Call<ModelWeather> call, Response<ModelWeather> response) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "onResponse Status", Toast.LENGTH_SHORT).show();
                ModelWeather modelWeather = response.body();
                WeatherObservation weatherObservation = modelWeather.getWeatherObservation();
                Integer dataObs = weatherObservation.getElevation();
                Double dataObs2 = weatherObservation.getLng();
                String dataObs3 = weatherObservation.getObservation();
                String dataObs4 = weatherObservation.getiCAO();
                String dataObs5 = weatherObservation.getClouds();
                String dataObs6 = weatherObservation.getDewPoint();
                String dataObs7 = weatherObservation.getCloudsCode();
                String dataObs8 = weatherObservation.getDatetime();
                String dataObs9 = weatherObservation.getCountryCode();
                String dataObs10 = weatherObservation.getTemperature();
                elevationMain.setText(dataObs.toString());
                lngMain.setText(String.valueOf(dataObs2));
                observationMain.setText(dataObs3);
                ICAO.setText(dataObs4);
                clouds.setText(dataObs5);
                dewPoint.setText(dataObs6);
                cloudsCode.setText(dataObs7);
                datetime.setText(dataObs8);
                countryCode.setText(dataObs9);
                temperature.setText(dataObs10);
            }

            @Override
            public void onFailure(Call<ModelWeather> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "on Failure", Toast.LENGTH_SHORT).show();
            }
        });
        /*service.getData("http://api.geonames.org/weatherIcaoJSON?ICAO=LSZH&username=demos").enqueue(new Callback<ModelWeather>() {
            @Override
            public void onResponse(Call<ModelWeather> call, Response<ModelWeather> response) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "onResponse Status", Toast.LENGTH_SHORT).show();
                ModelWeather modelWeather = response.body();
                WeatherObservation weatherObservation = modelWeather.getWeatherObservation();
                Integer dataObs = weatherObservation.getElevation();
                Double dataObs2 = weatherObservation.getLng();
                String dataObs3 = weatherObservation.getObservation();
                String dataObs4 = weatherObservation.getiCAO();
                String dataObs5 = weatherObservation.getClouds();
                String dataObs6 = weatherObservation.getDewPoint();
                String dataObs7 = weatherObservation.getCloudsCode();
                String dataObs8 = weatherObservation.getDatetime();
                String dataObs9 = weatherObservation.getCountryCode();
                String dataObs10 = weatherObservation.getTemperature();
                elevationMain.setText(dataObs.toString());
                lngMain.setText(String.valueOf(dataObs2));
                observationMain.setText(dataObs3);
                ICAO.setText(dataObs4);
                clouds.setText(dataObs5);
                dewPoint.setText(dataObs6);
                cloudsCode.setText(dataObs7);
                datetime.setText(dataObs8);
                countryCode.setText(dataObs9);
                temperature.setText(dataObs10);
            }

            @Override
            public void onFailure(Call<ModelWeather> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "on Failure", Toast.LENGTH_SHORT).show();
            }
        });*/
    }
/*
    private void showRecyView(List<DataWeather> dataWeathers) {
        itemCategory.setLayoutManager(new LinearLayoutManager(this,
                RecyclerView.VERTICAL,
                false));
        weatherAdapter = new WeatherAdapter(this,dataWeathers);
        itemCategory.setAdapter((RecyclerView.Adapter) weatherAdapter);
    }*/
}
